window.addEventListener("load",logout);

function logout(){
    while(sessionStorage.getItem("auth-token")){
        sessionStorage.removeItem("auth-token");
    }
    showLogin();
}