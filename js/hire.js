window.addEventListener("load",registerForm);

function registerForm(){
    document.getElementById("hire-form").addEventListener("submit",addEmployee);
}

function addEmployee(event){
    event.preventDefault();
    const payload = new FormData();
    payload.append("firstName",document.getElementById("firstName").value);
    payload.append("lastName",document.getElementById("lastName").value);
    payload.append("payRate",document.getElementById("payRate").value);
    payload.append("federalID",document.getElementById("fid").value);
    if(sessionStorage.key("auth-token"))
        authToken = sessionStorage.getItem("auth-token");
    else
        authToken = "UNAUTHORIZED!";
    sendAjaxPost(backendAddr+"/employees",addEmployeeSuccess,addEmployeeFailure,payload,authToken);
}

function addEmployeeSuccess(xhr){
    document.getElementById("firstName").value="";
    document.getElementById("firstName").focus();
    document.getElementById("lastName").value="";
    document.getElementById("payRate").value="";
    document.getElementById("fid").value="";
    let statusMessage = document.getElementById("status-message");
    statusMessage.innerText = "Employee Added Successfully!";
    displayGreen(statusMessage,5000);
}

function addEmployeeFailure(xhr){
    let statusMessage = document.getElementById("status-message");
    statusMessage.innerText = "Error encountered, Employee not Added";
    displayRed(statusMessage,5000);
}

function displayGreen(domElement,timeout){
    domElement.classList.add("status-message-green");
    domElement.classList.remove("invisible");
    setTimeout(hideStatusMessage, timeout);
}

function displayRed(domElement,timeout){
    domElement.classList.add("status-message-red");
    domElement.classList.remove("invisible");
    setTimeout(hideStatusMessage, timeout);
}

function hideStatusMessage(){
    document.getElementById("status-message").classList.add("invisible");
    document.getElementById("status-message").classList.remove("status-message-green");
    document.getElementById("status-message").classList.remove("status-message-red");
}
