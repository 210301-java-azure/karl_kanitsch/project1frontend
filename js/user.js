window.addEventListener("load",loadDetails);

function loadDetails(){
    sendAjaxAuthenticateRequest(userFound,userNotFound);
}

function userFound(xhr){
    let claimsMap = JSON.parse(JSON.parse(xhr.response));
    sendAjaxGetRecordsRequest(backendAddr+"/users/"+claimsMap.userID,populateUserDetail,showGetUserError);
}

function userNotFound(xhr){

}

function populateUserDetail(xhr){
    const detailCard = document.getElementById("user-detail");
    let user = JSON.parse(xhr.response);
    document.getElementById("usernameHeader").innerText=user.username;
    document.getElementById("uid").value=user.userID;
    document.getElementById("username").value=user.username;
    document.getElementById("email").value=user.email;
    if(user.roleID == 3){
        document.getElementById("roleID").value="System Administrator";
    } else if(user.roleID == 2){
        document.getElementById("roleID").value="HR Manager";
    } else if(user.roleID == 1){
        document.getElementById("roleID").value="User";
    }
    
    document.getElementById("employeeID").value=user.employeeID;
}

function showGetUserError(){

}
