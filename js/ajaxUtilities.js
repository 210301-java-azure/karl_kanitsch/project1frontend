const backendAddr = 'http://52.150.14.143';
// const backendAddr = "http://localhost";

function sendAjaxRequest(method, url, successCallback, failureCallback, authToken, body){
    const xhr = new XMLHttpRequest();
    xhr.open(method,url);
    if(authToken)
        xhr.setRequestHeader("auth-token", authToken);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status>=200 && xhr.status<400){
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxGet(url, successCallback, failureCallback, authToken){
    sendAjaxRequest("GET",url,successCallback,failureCallback,authToken);
}

function sendAjaxPost(url, successCallback, failureCallback, payload, authToken){
    sendAjaxRequest("POST",url,successCallback,failureCallback,authToken,payload);
}

function sendAjaxPut(url, successCallback, failureCallback, payload, authToken){
    sendAjaxRequest("PUT",url,successCallback,failureCallback,authToken,payload);
}

function sendAjaxLoginRequest(username, password, successCallback, failureCallback){
    // const payload = `username=${username}&password=${password}`;
    // console.log(payload);
    const payload = new FormData();
    payload.append("username",username);
    payload.append("password",password);
    sendAjaxPost(backendAddr+"/login", successCallback, failureCallback, payload, null);
}

function sendAjaxAuthenticateRequest(successCallback,failureCallback) {
    // console.log("sending authenticateRequest");
    const authToken = sessionStorage.getItem("auth-token");
    if(successCallback && failureCallback){
        sendAjaxGet(backendAddr+"/login",successCallback,failureCallback,authToken);
    } else {
        sendAjaxGet(backendAddr+"/login",showLogOut,showLogin,authToken);
    }
}

function sendAjaxRegisterNewUserRequest(username, password, email, successCallback, failureCallback){
    const payload = new FormData();
    payload.append("username",username);
    payload.append("password",password);
    payload.append("email",email);
    sendAjaxPost(backendAddr+"/register",successCallback, failureCallback, payload);
}

function sendAjaxGetRecordsRequest(url,successCallback,failureCallback){
    if(sessionStorage.key("auth-token"))
        authToken = sessionStorage.getItem("auth-token");
    else
        authToken = "UNAUTHORIZED!";
    sendAjaxGet(url,successCallback,failureCallback,authToken);
}

function sendAjaxUpdateEmployeeRequest(url,data,successCallback,failureCallback){
    const payload = new FormData();
    payload.append("operation",data.operation);
    payload.append("eid",data.employeeID);
    payload.append("firstName",data.firstName);
    payload.append("lastName",data.lastName);
    payload.append("payRate",data.payRate);
    payload.append("accruedPTO",data.accruedPTO);
    let authToken = "";
    if(sessionStorage.key("auth-token"))
        authToken = sessionStorage.getItem("auth-token");
    else
        authToken = "UNAUTHORIZED!";
    sendAjaxPut(url,successCallback,failureCallback,payload,authToken);
}

function sendAjaxTerminateEmployeeRequest(url,eid,successCallback,failureCallback){
    const payload = new FormData();
    payload.append("operation","terminate");
    payload.append("eid",eid);
    let authToken = "";
    if(sessionStorage.key("auth-token"))
        authToken = sessionStorage.getItem("auth-token");
    else
        authToken = "UNAUTHORIZED!";
    sendAjaxPut(url,successCallback,failureCallback,payload,authToken)
}


