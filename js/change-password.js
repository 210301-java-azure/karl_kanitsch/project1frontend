let specialChars = [",",".","!","@","#","*"];
let specialCharsString = "[";

window.addEventListener("load", function() {
    document.getElementById("change-password-form").addEventListener("submit",changePassword);
    document.getElementById("new-password").addEventListener("blur",checkPasswords);
    document.getElementById("new-password_confirmation").addEventListener("change",checkPasswords);
    for(let char of specialChars){
        specialCharsString += char + "  ";
    }
    specialCharsString += "]";
    document.getElementById("special_char_warning").innerText="Passwords must contain at least 1 special character: "+specialCharsString;
    sendAjaxAuthenticateRequest(userFound,userNotFound);
});

function userFound(xhr){
    let claimsMap = JSON.parse(JSON.parse(xhr.response));
    document.getElementById("username").value = claimsMap.username;
    document.getElementById("userID").value = claimsMap.userID;
}

function userNotFound(){};

function changePassword(event){
    event.preventDefault();
    const payload = new FormData();
    payload.append("username",document.getElementById("username").value);
    payload.append("password",document.getElementById("password").value);
    if(document.getElementById("new-password").value === document.getElementById("new-password_confirmation").value) {
        payload.append("new-password",document.getElementById("new-password").value);
        sendAjaxPost(backendAddr+"/users/"+document.getElementById("userID").value+"/change-password", changePasswordSuccess, changePasswordFailure,payload);
    }
    
}

function changePasswordSuccess(){
    location.href="/logout.html";
}

function changePasswordFailure(){
    console.log("registration failure");
}

function checkPasswords(){
    const password1 = document.getElementById("new-password").value;
    const password2 = document.getElementById("new-password_confirmation").value;
    let hasGoodLength = false;
    let hasSpecialChar = false;
    let hasBothCases = false;
    let numberCheck = hasNumbers(password1);
    let passwordsMatch = false;
    if(password1.length >= 8){
        hasGoodLength = true;
    }
    if(hasGoodLength){
        document.getElementById("password_length_warning").hidden = true;
    } else {
        document.getElementById("password_length_warning").hidden = false;
    }

    for(let char in specialChars){
        if(password1.includes(char)){
            hasSpecialChar = true;
        }
    }
    if(hasSpecialChar){
        document.getElementById("special_char_warning").hidden = true;
    } else {
        document.getElementById("special_char_warning").hidden = false;
    }
        
    if(hasLowerCase(password1) && hasUpperCase(password1)){
        hasBothCases = true;
    }
    if(hasBothCases){
        document.getElementById("password_case_warning").hidden = true;
    } else {
        document.getElementById("password_case_warning").hidden = false;
    }

    if(numberCheck) {
        document.getElementById("password_number_warning").hidden = true;
    } else {
        document.getElementById("password_number_warning").hidden = false;
    }

    if(password1===password2){
        passwordsMatch = true;
    }
    if(passwordsMatch){
        document.getElementById("password_match_warning").hidden = true;
    } else {
        document.getElementById("password_match_warning").hidden = false;
    }
    
    if(hasGoodLength && hasSpecialChar && hasBothCases && numberCheck && passwordsMatch){
        document.getElementById("submit_btn").disabled = false;
    }
}

function hasLowerCase(string){
    return string.toUpperCase() != string;
}

function hasUpperCase(string){
    return string.toLowerCase() != string;
}

function hasNumbers(string) {
    return (/[0-9]/.test(string));
}



