
// console.log("navbar.js run");
fetch("./components/_navbar.html")
    .then(response => {
        return response.text();
    })
    .then(data => {
        const parser = new DOMParser();
        const htmlDocument = parser.parseFromString(data,"text/html");
        const navbar = htmlDocument.querySelector("nav");
        document.querySelector("header").appendChild(navbar);
        hamburgerListener();
    });


// console.log("footer.js run");
fetch("./components/_footer.html")
    .then(response => {
        return response.text();
    })
    .then(data => {
        const parser = new DOMParser();
        const htmlDocument = parser.parseFromString(data,"text/html");
        const footer = htmlDocument.querySelector(".footer");
        document.querySelector("footer").appendChild(footer);
    });
