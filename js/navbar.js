window.addEventListener("load",checkLoginStatus);

function checkLoginStatus(){
    sendAjaxAuthenticateRequest();
}

function showLogin(){
    document.getElementById("login-link").classList.remove("toggle-hidden");
    document.getElementById("logout-link").classList.add("toggle-hidden");
}

function showLogOut(){
    document.getElementById("logout-link").classList.remove("toggle-hidden");
    document.getElementById("login-link").classList.add("toggle-hidden");
}
