window.addEventListener("load",populateUsersTable);

function populateUsersTable(){
    document.getElementById("users-table").textContent = "";
    document.getElementById("user-detail-form").addEventListener("submit",updateUser);
    sendAjaxGetRecordsRequest(backendAddr+"/users",usersGetSuccess,usersGetFailure);
}

function usersGetSuccess(xhr){
    // console.log("usersGetSuccess");
    const users = JSON.parse(xhr.response);
    // console.log(users);
    for(let user of users){
        // console.log(user);
        addUserRow(user);
    }
}

function usersGetFailure(xhr){
    console.log("usersGetFailure");
}

function addUserRow(user){
    const table = document.getElementById("users-table");
    let row = document.createElement("tr");
    let uid = user.userID;
    row.addEventListener("click",()=>showUserDetail(uid));

    let id = document.createElement("td");
    id.innerText = user.userID;
    row.appendChild(id);
    
    let username = document.createElement("td");
    username.innerText = user.username;
    row.appendChild(username);

    let email = document.createElement("td");
    email.innerText = user.password;
    row.appendChild(email);

    let roleID = document.createElement("td");
    roleID.innerText = user.roleID;
    row.appendChild(roleID);

    let eid = document.createElement("td");
    eid.innerText = user.employeeID;
    row.appendChild(eid);

    table.appendChild(row);
}

function showUserDetail(uid){
    const table = document.getElementById("users-table");
    table.hidden = true;
    sendAjaxGetRecordsRequest(backendAddr+"/users/"+uid,populateUserDetail,showGetUserError);
}

function hideUserDetail(){
    const detailCard = document.getElementById("user-detail");
    detailCard.hidden = true;
    const table = document.getElementById("users-table");
    table.hidden = false;
    populateUsersTable();
}

function populateUserDetail(xhr){
    const detailCard = document.getElementById("user-detail");
    detailCard.hidden = false;
    document.getElementsByClassName("close-button")[0].addEventListener("click",hideUserDetail);
    let user = JSON.parse(xhr.response);
    document.getElementById("usernameHeader").innerText=user.username;
    document.getElementById("uid").value=user.userID;
    document.getElementById("username").value=user.username;
    document.getElementById("email").value=user.email;
    document.getElementById("roleID").value=user.roleID;
    document.getElementById("employeeID").value=user.employeeID;
}

function showGetUserError(){
    console.log("Error encountered in AJAX get");
}

function updateUser(event){
    event.preventDefault();
    const payload = new FormData();
    payload.append("userID",document.getElementById("uid").value);
    payload.append("employeeID",document.getElementById("employeeID").value);
    payload.append("roleID",document.getElementById("roleID").value);
    if(sessionStorage.key("auth-token"))
        authToken = sessionStorage.getItem("auth-token");
    else
        authToken = "UNAUTHORIZED!";
    sendAjaxPost(backendAddr+"/users",updateUserSuccess,updateUserFailure,payload,authToken);
}

function updateUserSuccess(xhr){
    // console.log("User Account updated successfully");
    hideUserDetail();
}

function updateUserFailure(xhr){
    console.log(xhr.response);
}


