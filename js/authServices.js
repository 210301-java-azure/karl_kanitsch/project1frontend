
function authenticateUser(event){
    event.preventDefault();
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    sendAjaxLoginRequest(username, password, loginSuccess, loginFailure);
}

function checkAuth(){
    if(sessionStorage.getItem("auth-token")){
        sendAjaxAuthenticateRequest();
    } else {
        return false;
    }
}